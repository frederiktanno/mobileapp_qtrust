package frederiktanno.com.laurensiusfrederiktanno.Services;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;

import frederiktanno.com.laurensiusfrederiktanno.Model.MealModel;
import frederiktanno.com.laurensiusfrederiktanno.View.MainActivity;

public class APIRetriever extends AsyncTask<Void,String,String>{

    private MainActivity activity;
    private URL url;
    //private Context context;
    private HttpURLConnection connection;
    private MealModel meal;
    private JSONObject root;
    private JSONArray meals;
    private InputStream reader;
    private Context context;
    private ProgressDialog pDialog;
    private String apiurl;
    private MealServices mealServices;
    private static final String LOG_TAG=APIRetriever.class.getName();
    private ArrayList<MealModel> mealsList = new ArrayList<>();

    public APIRetriever(MainActivity activity, String apiurl){
        this.activity = activity;
        this.context = activity.getApplicationContext();
        this.apiurl = apiurl;
        this.mealServices = MealServices.getSingletonInstance(context);
    }

    @Override
    protected void onPreExecute(){
        super.onPreExecute();
        //display a progress dialog
        pDialog = new ProgressDialog(activity);
        pDialog.setMessage("Fetching data from the internet...");
        pDialog.setCancelable(false);
        pDialog.show();
    }

    @Override
    protected void onPostExecute(String e){
        super.onPostExecute(e);
        //remove the progress dialog
        if (pDialog.isShowing())
            pDialog.dismiss();
        if(e!=null){
            Log.i(LOG_TAG,"adding data into meals list");
            mealServices.setMeals(mealsList);
            activity.setRecyclerView();
        }

    }

    @Override
    protected String doInBackground(Void...params){
        String response;
        try{
            //create the URL object
            url = new URL(apiurl);
            connection = (HttpURLConnection) url.openConnection();
            //initiate a connection with API provider, if connection is refused, display
            //a Toast and return null
            if(connection==null){
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(activity.getApplicationContext(),
                                "Failed to initialize connection with API provider",
                                Toast.LENGTH_LONG)
                                .show();
                    }
                });
                return null;
            }
            reader = new BufferedInputStream(connection.getInputStream());
            //read the HTTP response from API provider using buffer reader
            response = readResponse(reader);
            //parse the response into JSON, and persist the data into memory by creating MealModel objects
            if(response!=null){
                Log.i(LOG_TAG,"Responses: "+response);
                root = new JSONObject(response);
                meals = root.getJSONArray("meals");
                for(int i=0; i<meals.length();i++){
                    JSONObject child = meals.getJSONObject(i);
                    meal = new MealModel();
                    meal.setId(child.getString("idMeal"));
                    meal.setName(child.getString("strMeal"));
                    meal.setThumbnailURL(child.getString("strMealThumb"));

                    //add the meal objects into a list of meals
                    mealsList.add(meal);
                }
            }
            else{
                Log.e(LOG_TAG, "Couldn't get json from server.");
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(activity.getApplicationContext(),
                                "Couldn't get json from server. Check LogCat for possible errors!",
                                Toast.LENGTH_LONG)
                                .show();
                    }
                });
                return null;
            }


        }
        catch(MalformedURLException ex){
            Log.e(LOG_TAG,ex.toString());
            ex.printStackTrace();
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(activity.getApplicationContext(),
                            "Failed to initialize connection with API provider",
                            Toast.LENGTH_LONG)
                            .show();
                }
            });
            return null;
        } catch(UnknownHostException ex){
            Log.e(LOG_TAG,ex.toString());
            ex.printStackTrace();
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(activity.getApplicationContext(),
                            "Failed to access URL. Please check your Internet Connection",
                            Toast.LENGTH_LONG)
                            .show();
                }
            });
            return null;
        } catch(IOException ex){
            Log.e(LOG_TAG,ex.toString());
            ex.printStackTrace();
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(activity.getApplicationContext(),
                            "JSON parse error",
                            Toast.LENGTH_LONG)
                            .show();
                }
            });
            return null;
        }
        catch(JSONException ex){
            Log.e(LOG_TAG,ex.toString());
            ex.printStackTrace();
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(activity.getApplicationContext(),
                            "JSON parse error",
                            Toast.LENGTH_LONG)
                            .show();
                }
            });
            return null;
        } catch(Exception ex){
            Log.e(LOG_TAG,ex.toString());
            ex.printStackTrace();
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(activity.getApplicationContext(),
                            "JSON parse error",
                            Toast.LENGTH_LONG)
                            .show();
                }
            });
            return null;
        }
        return "OK";
    }

    private String readResponse(InputStream reader){
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(reader));
        StringBuilder builder = new StringBuilder();
        try{
            String line;
            while((line = bufferedReader.readLine())!=null){
                builder.append(line);
            }

        } catch (IOException e) {
            Log.e(LOG_TAG,e.toString());
            e.printStackTrace();
            return null;
        }
        return builder.toString();
    }
}
