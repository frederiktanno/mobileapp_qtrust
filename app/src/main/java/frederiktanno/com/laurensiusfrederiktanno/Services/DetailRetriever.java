package frederiktanno.com.laurensiusfrederiktanno.Services;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import frederiktanno.com.laurensiusfrederiktanno.Model.MealModel;
import frederiktanno.com.laurensiusfrederiktanno.View.DetailActivity;
import frederiktanno.com.laurensiusfrederiktanno.View.MainActivity;
import frederiktanno.com.laurensiusfrederiktanno.View.QRScannerActivity;

public class DetailRetriever extends AsyncTask<Void,String,String> {
    private DetailActivity activity;
    private URL url;
    private HttpURLConnection connection;
    private MealModel meal;
    private JSONObject root;
    private JSONArray meals;
    private InputStream reader;
    private Context context;
    private ProgressDialog pDialog;
    private String apiurl;
    private MealServices mealServices;
    private static final String LOG_TAG = DetailRetriever.class.getName();

    public DetailRetriever(String url, DetailActivity activity){
        this.apiurl = url;
        this.activity = activity;
        this.context = activity.getApplicationContext();
        this.mealServices = MealServices.getSingletonInstance(this.context);
    }

    @Override
    protected void onPreExecute(){
        super.onPreExecute();
        //display a progress dialog
        pDialog = new ProgressDialog(activity);
        pDialog.setMessage("Fetching data from the internet...");
        pDialog.setCancelable(false);
        pDialog.show();
    }

    @Override
    protected void onPostExecute(String e){
        super.onPostExecute(e);
        //remove the progress dialog
        if (pDialog.isShowing())
            pDialog.dismiss();
        if(e!=null){
            activity.setModel(meal);
            activity.setText();
            //activity.setRecyclerView();
        }
        if(e==null ||e.equals("Data not found") ){
            Log.e(LOG_TAG, "Data not found.");
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(activity.getApplicationContext(),
                            "Data not found",
                            Toast.LENGTH_LONG)
                            .show();
                }
            });
            activity.finish();
            activity.startActivity(new Intent(context,QRScannerActivity.class));
        }

    }

    @Override
    protected String doInBackground(Void...params){
        String response;
        try{
            //create the URL object
            url = new URL(apiurl);
            connection = (HttpURLConnection) url.openConnection();
            //initiate a connection with API provider, if connection is refused, display
            //a Toast and return null
            if(connection==null){
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(activity.getApplicationContext(),
                                "Failed to initialize connection with API provider",
                                Toast.LENGTH_LONG)
                                .show();
                    }
                });
                return null;
            }
            reader = new BufferedInputStream(connection.getInputStream());
            //read the HTTP response from API provider using buffer reader
            response = readResponse(reader);
            //parse the response into JSON, and persist the data into memory by creating MealModel objects
            if(response!=null){
                Log.i(LOG_TAG,"Responses: "+response);
                root = new JSONObject(response);
                meals = root.getJSONArray("meals");
                JSONObject child = meals.getJSONObject(0);
                parseJSONArray(child);
            }
            else{
                //if the QR code scanned was invalid

                return "Data not found";
            }


        }
        catch(MalformedURLException ex){
            Log.e(LOG_TAG,ex.toString());
            ex.printStackTrace();
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(activity.getApplicationContext(),
                            "Failed to initialize connection with API provider",
                            Toast.LENGTH_LONG)
                            .show();
                }
            });
            return null;
        } catch(UnknownHostException ex){
            Log.e(LOG_TAG,ex.toString());
            ex.printStackTrace();
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(activity.getApplicationContext(),
                            "Failed to access URL. Please check your Internet Connection",
                            Toast.LENGTH_LONG)
                            .show();
                }
            });
            return null;
        } catch(IOException ex){
            Log.e(LOG_TAG,ex.toString());
            ex.printStackTrace();
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(activity.getApplicationContext(),
                            "JSON parse error",
                            Toast.LENGTH_LONG)
                            .show();
                }
            });
            return null;
        }
        catch(JSONException ex){
            Log.e(LOG_TAG,ex.toString());
            ex.printStackTrace();
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(activity.getApplicationContext(),
                            "JSON parse error",
                            Toast.LENGTH_LONG)
                            .show();
                }
            });
            return null;
        } catch(Exception ex){
            Log.e(LOG_TAG,ex.toString());
            ex.printStackTrace();
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(activity.getApplicationContext(),
                            "JSON parse error",
                            Toast.LENGTH_LONG)
                            .show();
                }
            });
            return null;
        }
        return "OK";
    }

    private void parseJSONArray(JSONObject child) throws JSONException {
        Iterator<String> iter = child.keys();
        ArrayList<String>ingredients = new ArrayList<>();
        ArrayList<String>measures = new ArrayList<>();

        String id = child.getString("idMeal");
        String name = child.getString("strMeal");
        String drinkAlternate = child.getString("strDrinkAlternate");
        String category = child.getString("strCategory");
        String area =child.getString("strArea");
        String instructions =child.getString("strInstructions");
        String thumbnailURL =child.getString("strMealThumb");
        String ytURL = child.getString("strYoutube");
        String tags = child.getString("strTags");

        while(iter.hasNext()){

            String key = iter.next();
            if(key.contains("strIngredient")){

                ingredients.add(child.getString(key));
            }
            if(key.contains("strMeasure")){
                measures.add(child.getString(key));
            }
        }
        Log.i(LOG_TAG,"size: "+ingredients.size());
        Log.i(LOG_TAG,"size: "+measures.size());
        meal = mealServices.getMealById(id);
        if(meal!=null){
            Log.i(LOG_TAG,"populating model");
            meal.setThumbnailURL(thumbnailURL);
            meal.setArea(area);
            meal.setCategory(category);
            meal.setDrinkAlternate(drinkAlternate);
            meal.setIngredients(ingredients);
            meal.setMeasures(measures);
            meal.setTags(tags);
            meal.setInstructions(instructions);
            meal.setYtURL(ytURL);
            meal.setName(name);
        }
    }


    private String readResponse(InputStream reader){
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(reader));
        StringBuilder builder = new StringBuilder();
        try{
            String line;
            while((line = bufferedReader.readLine())!=null){
                builder.append(line);
            }

        } catch (IOException e) {
            Log.e(LOG_TAG,e.toString());
            e.printStackTrace();
            return null;
        }
        return builder.toString();
    }
}
