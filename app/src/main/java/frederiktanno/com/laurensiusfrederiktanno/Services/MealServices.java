package frederiktanno.com.laurensiusfrederiktanno.Services;

import android.content.Context;

import java.util.ArrayList;

import frederiktanno.com.laurensiusfrederiktanno.Model.MealModel;

public class MealServices {
    private ArrayList<MealModel> meals = new ArrayList<>();
    private static Context context;
    private MealModel meal;

    private MealServices(){

    }

    private static class LazyHolder{
        static final MealServices INSTANCE = new MealServices();
    }

    public static MealServices getSingletonInstance(Context context){
        MealServices.context = context;
        return LazyHolder.INSTANCE;
    }

    public void setMeals(ArrayList<MealModel> meals){
        this.meals = meals;
    }

    public MealModel getMealById(String id){
        for(int i=0; i<meals.size(); i++){
            if(meals.get(i).getId().equals(id)){
                return meals.get(i);
            }
        }
        return null;
    }

    public ArrayList<MealModel> getMeals() {
        return meals;
    }
}
