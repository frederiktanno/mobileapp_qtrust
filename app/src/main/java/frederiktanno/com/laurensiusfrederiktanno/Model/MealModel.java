package frederiktanno.com.laurensiusfrederiktanno.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class MealModel implements Serializable {
    private String id;
    private String name;
    private String drinkAlternate;
    private String category;
    private String area;
    private String instructions;
    private String thumbnailURL;
    private String ytURL;
    private String tags;
    private ArrayList<String> ingredients;
    private ArrayList<String> measures;

    public MealModel(){

    }

    public MealModel(String id, String name, String drinkAlternate, String category, String area,
                     String instructions, String thumbnailURL, String ytURL, String tags,
                     ArrayList<String> ingredients, ArrayList<String> measures ){
        this.id = id;
        this.name = name;
        this.drinkAlternate = drinkAlternate;
        this.category = category;
        this.area = area;
        this.instructions = instructions;
        this.thumbnailURL = thumbnailURL;
        this.ytURL = ytURL;
        this.tags = tags;
        this.ingredients = ingredients;
        this.measures = measures;
    }

    public String getName(){
        return this.name;
    }
    public String getId(){return this.id;}

    public String getArea() {
        return area;
    }

    public String getCategory() {
        return category;
    }

    public String getDrinkAlternate() {
        return drinkAlternate;
    }

    public String getInstructions() {
        return instructions;
    }

    public String getThumbnailURL() {
        return thumbnailURL;
    }

    public String getYtURL() {
        return ytURL;
    }

    public ArrayList<String> getIngredients() {
        return ingredients;
    }

    public ArrayList<String> getMeasures() {
        return measures;
    }

    public String getTags() {
        return tags;
    }

    public void setId(String id){
        this.id =id;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setThumbnailURL(String url){
        this.thumbnailURL = url;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setDrinkAlternate(String drinkAlternate) {
        this.drinkAlternate = drinkAlternate;
    }

    public void setIngredients(ArrayList<String> ingredients) {
        this.ingredients = ingredients;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public void setMeasures(ArrayList<String> measures) {
        this.measures = measures;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public void setYtURL(String ytURL) {
        this.ytURL = ytURL;
    }
}
