package frederiktanno.com.laurensiusfrederiktanno.Controller;

import android.content.Context;
import android.graphics.Rect;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import frederiktanno.com.laurensiusfrederiktanno.R;

public class RecyclerViewDecorationController extends RecyclerView.ItemDecoration {
    private int space;
    private DividerItemDecoration div;

    public RecyclerViewDecorationController(int space, DividerItemDecoration div) {
        this.space = space;
        this.div = div;
    }

    public static RecyclerViewDecorationController createDecoration(Context context) {

        int spacingInPixels = context.getResources().getDimensionPixelSize(R.dimen.item_margin);
        DividerItemDecoration div = new DividerItemDecoration(context, LinearLayoutManager.VERTICAL);
        return new RecyclerViewDecorationController(spacingInPixels,div);
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {

        outRect.top = space;
        outRect.left = space;
        outRect.right = space;
        outRect.bottom = space;
    }
}
