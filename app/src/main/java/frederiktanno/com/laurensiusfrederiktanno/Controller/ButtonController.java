package frederiktanno.com.laurensiusfrederiktanno.Controller;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.content.Context;

import frederiktanno.com.laurensiusfrederiktanno.View.QRScannerActivity;

public class ButtonController implements Button.OnClickListener {
    Context context;

    public ButtonController(Context context){
        this.context = context;
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(this.context, QRScannerActivity.class);
        this.context.startActivity(intent);
    }
}
