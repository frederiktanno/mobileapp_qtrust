package frederiktanno.com.laurensiusfrederiktanno.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import frederiktanno.com.laurensiusfrederiktanno.Model.MealModel;
import frederiktanno.com.laurensiusfrederiktanno.R;
import frederiktanno.com.laurensiusfrederiktanno.Services.MealServices;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MainViewHolder> {

    private ArrayList<MealModel> meals;
    Context context;
    private MealServices services;
    private static final String LOG_TAG = RecyclerViewAdapter.class.getName();

    public static class MainViewHolder extends RecyclerView.ViewHolder{
        public TextView meal;
        public MainViewHolder(View v){
            super(v);
            meal = v.findViewById(R.id.mealTitle);
        }
    }

    public RecyclerViewAdapter(Context context){
        this.context = context;
        Log.v(LOG_TAG,"test");
        services = MealServices.getSingletonInstance(this.context);
        this.meals = getMeals();
        Log.v(LOG_TAG,"size: "+this.meals.size());
    }

    @Override
    public MainViewHolder onCreateViewHolder(ViewGroup parent,int viewType) {
        // create a new view
       View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.main_layout,parent,false);

        return new MainViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MainViewHolder holder, int position) {

        String name = meals.get(position).getName();
        holder.meal.setText(name);

    }

    @Override
    public int getItemCount() {
        return meals.size();
    }

    public ArrayList<MealModel>getMeals(){
        return this.services.getMeals();
    }


}
