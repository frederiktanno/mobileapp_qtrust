package frederiktanno.com.laurensiusfrederiktanno.View;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

import frederiktanno.com.laurensiusfrederiktanno.Model.MealModel;
import frederiktanno.com.laurensiusfrederiktanno.R;
import frederiktanno.com.laurensiusfrederiktanno.Services.DetailRetriever;

public class DetailActivity extends AppCompatActivity{

    private static final String LOG_TAG = DetailActivity.class.getName();
    private DetailRetriever retrieve;
    private TextView title, mainTitle, description, descriptionText;
    private TextView area,areaText, tags, tagsText,youtube,youtubeText;
    private TextView ingredients,measures,measuresText;
    private Spinner ingredientsSpinner;
    private MealModel model;
    ArrayAdapter<String> adapter;
    private String url = "https://www.themealdb.com/api/json/v1/1/lookup.php?i=";
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        //retrieve the ID value from the scanned barcode
        String id = getIntent().getStringExtra("ID");
        url = url.concat(id);
        Log.i(LOG_TAG,url);
        retrieve = new DetailRetriever(url,this);
        retrieve.execute();
        title = findViewById(R.id.title);
        mainTitle = findViewById(R.id.mainTitle);
        description = findViewById(R.id.category);
        descriptionText = findViewById(R.id.categoryText);
        area = findViewById(R.id.area);
        areaText = findViewById(R.id.areaText);
        tags = findViewById(R.id.tags);
        tagsText = findViewById(R.id.tagsText);
        youtube = findViewById(R.id.youtube);
        youtubeText = findViewById(R.id.youtubeText);
        ingredients = findViewById(R.id.ingredients);
        ingredientsSpinner = findViewById(R.id.ingredientsSpinner);
        measures = findViewById(R.id.measures);
        measuresText = findViewById(R.id.measuresText);

    }

    public void setModel(MealModel model){
        this.model = model;
    }

    @Override
    public void onResume(){
        super.onResume();


    }

    public void setText(){
        ArrayList<String> ingredientsList = model.getIngredients();
        final ArrayList<String>measuresList = model.getMeasures();
        Log.i(LOG_TAG,"setting text");
        title.setText(model.getName());
        mainTitle.setText(model.getName());
        description.setText("Category:");
        descriptionText.setText(model.getCategory());
        area.setText("Area:");
        areaText.setText(model.getArea());
        tags.setText("Tags:");
        tagsText.setText(model.getTags());
        youtube.setText("Youtube Video:");
        youtubeText.setText(model.getYtURL());
        ingredients.setText("Ingredients:");
        measures.setText("Measures:");
        adapter = new ArrayAdapter<>(this,R.layout.spinner_item_layout,ingredientsList);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        ingredientsSpinner.setAdapter(adapter);
        ingredientsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                measuresText.setText(measuresList.get(position));
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                return;
            }
        });
    }

}
