package frederiktanno.com.laurensiusfrederiktanno.View;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.CodeScannerView;
import com.budiyev.android.codescanner.DecodeCallback;
import com.google.zxing.Result;

import frederiktanno.com.laurensiusfrederiktanno.R;

public class QRScannerActivity extends AppCompatActivity {
    /*
    * QR Scanner library courtesy of Yuriy Budiyev and ZXing
    * Source: https://github.com/yuriy-budiyev/code-scanner
    * */
    private CodeScanner mCodeScanner;
    private QRScannerActivity activity = this;
    private CodeScannerView scannerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.qr_activity);
        scannerView = findViewById(R.id.scanner_view);
        //ask permission for camera usage
        if( ContextCompat.checkSelfPermission( this, android.Manifest.permission.CAMERA ) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(activity,new String[]{Manifest.permission.CAMERA},1);
        }
        else{
            setCamera();
        }
    }

    public void setCamera(){
        mCodeScanner = new CodeScanner(this, scannerView);
        mCodeScanner.setDecodeCallback(new DecodeCallback() {
            @Override
            public void onDecoded(@NonNull final Result result) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String id = result.getText();
                        //open details activity
                        Intent intent = new Intent(activity,DetailActivity.class);
                        intent.putExtra("ID",id);
                        activity.getApplicationContext().startActivity(intent);
                        Toast.makeText(activity, result.getText(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        scannerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCodeScanner.startPreview();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                          String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //permission granted
                    setCamera();

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(this, "Permission denied to use your camera", Toast.LENGTH_SHORT).show();
                    finish();
                    startActivity(new Intent(this,MainActivity.class));
                }
                return;
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(mCodeScanner!=null){
            mCodeScanner.startPreview();
        }

    }

    @Override
    protected void onPause() {
        if(mCodeScanner!=null){
            mCodeScanner.releaseResources();
        }

        super.onPause();
    }
}
