package frederiktanno.com.laurensiusfrederiktanno.View;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.widget.Button;

import frederiktanno.com.laurensiusfrederiktanno.Adapter.RecyclerViewAdapter;
import frederiktanno.com.laurensiusfrederiktanno.Controller.ButtonController;
import frederiktanno.com.laurensiusfrederiktanno.Controller.RecyclerViewDecorationController;
import frederiktanno.com.laurensiusfrederiktanno.R;
import frederiktanno.com.laurensiusfrederiktanno.Services.APIRetriever;
import frederiktanno.com.laurensiusfrederiktanno.Services.DatabaseHandler;
import frederiktanno.com.laurensiusfrederiktanno.Services.MealServices;

public class MainActivity extends AppCompatActivity {

    private static final String LOG_TAG=MainActivity.class.getName();
    private RecyclerView.LayoutManager myLayoutManager;
    private RecyclerView myRecyclerView;
    private APIRetriever api;
    public RecyclerViewAdapter adapter;
    private MealServices services;
    private DatabaseHandler db;
    //API URL for retrieving a list of meal data
    private String apiurl = "https://www.themealdb.com/api/json/v1/1/filter.php?c=Seafood";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        services = MealServices.getSingletonInstance(this);
        if(services.getMeals().isEmpty()){
            Log.i(LOG_TAG,"retrieving data from the API");
            api = new APIRetriever(MainActivity.this,apiurl);
            api.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
        else{
            //set Recycler View
            setRecyclerView();
        }

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Button button=new Button(this);
        button.setText("QR Scan");
        Toolbar.LayoutParams right=new Toolbar.LayoutParams(Toolbar.LayoutParams.WRAP_CONTENT, Toolbar.LayoutParams.WRAP_CONTENT);
        right.gravity= Gravity.END;
        button.setLayoutParams(right);
        toolbar.addView(button);

        addButtonListener(button);

    }

    @Override
    public void onRestart(){
        super.onRestart();
        if(services.getMeals().isEmpty()){
            Log.i(LOG_TAG,"retrieving data from the API");
            api = new APIRetriever(MainActivity.this,apiurl);
            api.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
        else{
            //set Recycler View
            setRecyclerView();
        }
    }

    public void addButtonListener(Button button){
        button.setOnClickListener(new ButtonController(this));
    }


    public void setRecyclerView(){
        myRecyclerView = findViewById(R.id.main_view);
        myRecyclerView.setHasFixedSize(true);

        //set Layout for Recycler View
        myLayoutManager = new LinearLayoutManager(this);
        myRecyclerView.setLayoutManager(myLayoutManager);

        //set Recycler View Margin and divider between items
        myRecyclerView.addItemDecoration(RecyclerViewDecorationController.createDecoration(this));

        //set view adapter
        adapter = new RecyclerViewAdapter(MainActivity.this);
        myRecyclerView.setAdapter(adapter);

    }
}
